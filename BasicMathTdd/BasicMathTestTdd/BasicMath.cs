﻿using System;

namespace BasicMathTestTdd
{
    public class BasicMath
    {
        public double Add(double v1, double v2) => v1 + v2;
        public double Sqrt(double value, double s) => Math.Sqrt(value);
        public double Divide(double v1, double v2) => v1 / v2;
        
    }
}
