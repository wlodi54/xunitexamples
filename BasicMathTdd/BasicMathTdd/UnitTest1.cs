﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BasicMathTestTdd;

namespace BasicMathTdd
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestAddMethod()
        {           
            BasicMath bm = new BasicMath();
            double res = bm.Add(21, 23);
            Assert.AreEqual(44, res);
        }

        [TestMethod]
        public void TestAddMethodFirstPlusSecondMinus()
        {
            BasicMath bm = new BasicMath();
            double res = bm.Add(2, -3);
            Assert.AreEqual(-1, res);
        }

        [TestMethod]
        public void TestAddMethodFirstMinusSecondPlus()
        {
            BasicMath bm = new BasicMath();
            double res = bm.Add(-2, 3);
            Assert.AreEqual(1, res);
        }

        [TestMethod]
        public void Sqrt()
        {
            double value = 9;
            int s = 3;
            BasicMath bm = new BasicMath();
            double res = bm.Sqrt(16, 2);
            Assert.AreEqual(4, res);
        }

        [DataRow(4,2,2.0)]
        [DataRow(4.12, 2.45, 2.0)]
        [TestMethod]
        public void DivideTest(double d1, double d2, double expected_result)
        {
            BasicMath bm = new BasicMath();
            double result = bm.Divide(d1, d2);
            Assert.AreEqual(expected_result, result);
        }

    }
}
