﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace SimpleNunit
{
    [TestFixture]
    public class TestingClass
    {
        [Test]
        public void TestCalculatorAdding
        {
        string sol = Calculator.Adding(10, 20);
        Assert.AreEqual("30",sol);
}
}
}
