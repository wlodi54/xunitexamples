﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace TestyCw
{
    [TestFixture]
    public class EnterToPage
    {
        [Test]
        public void EnterToPage_CheckUrl_CorrectlyValue()
        {
            IWebDriver driver = new ChromeDriver();
            var pageUrl = "http://www.adresbloga.pl";
            driver.Navigate().GoToUrl(pageUrl);
            driver.Manage().Window.Maximize();
            Assert.AreEqual(driver.Url, pageUrl);
            driver.Close();
        }
    }
}
