﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atata;
namespace SampleAppUiTest
{
    using _ = SingInPage;
    [Url("signin")]
    [VerifyTitle]
    [VerifyH1]
    public class SingInPage :Page<_>
    {
        public TextInput<_> Email { get; private set; }
        public PasswordInput<_> Password { get; private set; }
        public Button<UserPage,_> SingIn { get; private set; }
    }
}
