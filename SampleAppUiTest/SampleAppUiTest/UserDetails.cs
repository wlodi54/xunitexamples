﻿using Atata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleAppUiTest
{
    using _ = UserDetails;
    public class UserDetails:Page<_>
    {
        [FindFirst]
        public H1<_> Header { get; set; }

        [FindByDescriptionTerm]
        public Text<_> Email { get; set; }

        [FindByDescriptionTerm]
        public Content<Office,_> Office { get; set; }
        [FindByDescriptionTerm]
        public Content<Gender, _> Gender { get; set; }
        [FindByDescriptionTerm]
        public Content<DateTime?, _> Birthdat { get; set; }
        [FindByDescriptionTerm]
        public Text<_> Notes { get; set; }

    }
}
