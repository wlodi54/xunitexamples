﻿using Atata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleAppUiTest
{
    using _ = UserPage;
    [VerifyTitle]
    [VerifyH1]
    public class UserPage :Page<_>
    {
        public Button<UserEditWindow, _> New { get; private set; }
        public Table<UserTableRow, _> Users { get; private set; }
        public class UserTableRow : TableRow<_>
        {
            public Text<_> FirstName { get; set; }
            public Text<_> LastName { get; set; }
            public Text<_> Email { get; set; }
            public Content<Office,_> Office { get; set; }
            public Link<UserDetails,_> View { get; set; }
            public Button<UserEditWindow,_> Edit { get; set; }
            [CloseConfirmBox]
            public Button< _> Delete { get; set; }


        }
    }
}
