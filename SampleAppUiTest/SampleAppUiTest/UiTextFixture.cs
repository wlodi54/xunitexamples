﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atata;
using NUnit.Framework;
namespace SampleAppUiTest
{
    [TestFixture]
    public class UiTextFixture
    {
        [SetUp]
        public void SetUp()
        {
            AtataContext.Configure().
                UseChrome().WithArguments("start-maximized").
                UseBaseUrl("https://atata-framework.github.io/atata-sample-app/#!/").
                UseNUnitTestName().AddNUnitTestContextLogging().WithoutSectionFinish().LogNUnitError().Build();
        }
        [TearDown]
        public void TearDown()
        {
            AtataContext.Current?.CleanUp();
        }

        protected UserPage Login()
        {
            return Go.To<SingInPage>().
                Email.Set("admin@mail.com").
                Password.Set("abc123").
                SingIn.ClickAndGo();
        }
    }
}
