﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CatXUnitTest.Fixtures
{
    public class BaseFixture<TStartup> : IDisposable where TStartup : class
    {
        public HttpClient Client { get; }
        public TestServer Server { get; }

        public BaseFixture()
        {
            var builder = WebHost
                .CreateDefaultBuilder()
                .UseStartup<TStartup>()
                //Without this configuration may generate exceptions on consume dbcontext service
                .UseDefaultServiceProvider(options => options.ValidateScopes = false);

            Server = new TestServer(builder);
            Client = Server.CreateClient();
        }

        public void Dispose()
        {
            Client.Dispose();
            Server.Dispose();
        }
    }
}
